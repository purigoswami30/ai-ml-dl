#************** WAY 1 ***************

from pathlib import Path
import sys
path = str(Path(Path(__file__).parent.absolute()).parent.absolute())
sys.path.insert(0, path)

#************** WAY 2 ***************

# import sys
# sys.path.insert(0, '/Users/puri/Documents/MY_WORK/AI-ML-DL/ImportProject/')

#************** WAY 3 ***************


from Nested import module2

print(module2.sum(2,6))

module2.sayHi()