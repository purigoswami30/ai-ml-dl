import os

# Get the absolute path of the script's directory
script_dir = os.path.dirname(os.path.abspath(__file__))

# Construct the full file path
new_dir_path = os.path.join(script_dir, "package")

#CREATE NEW DIRECTORY
if not os.path.exists(new_dir_path):
    os.makedirs(new_dir_path)

#LIST FILE AND DIRECTORIES
print(os.listdir(script_dir))

#Joining Path
dirPath = "folder"
fileName = 'file.txt'
path = os.path.join(os.getcwd(), dirPath, fileName)
print(path)