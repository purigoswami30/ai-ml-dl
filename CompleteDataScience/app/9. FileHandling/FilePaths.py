import os

# Get the absolute path of the script's directory
script_dir = os.path.dirname(os.path.abspath(__file__))

# Construct the full file path
new_dir_path = os.path.join(script_dir, "package")

#CREATE NEW DIRECTORY
if not os.path.exists(new_dir_path):
    os.makedirs(new_dir_path)