import os

# Get the absolute path of the script's directory
script_dir = os.path.dirname(os.path.abspath(__file__))

# Construct the full file path
file_path = os.path.join(script_dir, "example.txt")
file_path_bin = os.path.join(script_dir, "example.bin")

# *** Read The Whole File ****
with open(file_path, "r") as file:
    content = file.read()
    #print(content)

# *** Read Line By Line ****
with open(file_path, "r") as file:
    for line in file:
        print(line.strip())
        
# *** Write The Whole File ****
# with open(file_path, "w") as file:
#     file.write("This is new line added\n")
#     file.write("This is last line")

# *** Write a File (Without Overiting) ****
with open(file_path, "a") as file:
    file.write("This is new line added\n")
    file.write("This is last line\n")


# *** Writing List of Lines ****
lines = ["Open the gate \n", "Make the gate \n",]
with open(file_path, "a") as file:
    file.writelines(lines)


# *** BINARY ****
some_bytes = b'\xC3\xA9 \x21'
with open(file_path_bin, "wb") as binary_file:
    binary_file.write(some_bytes)


with open(file_path_bin, "rb") as binary_file:
    print(binary_file.read())