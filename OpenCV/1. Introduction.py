# Importing the OpenCV library
import cv2
import sys

# Reading the image using imread() function
image = cv2.imread('tests.png')

# Extracting the height and width of an image
h, w = image.shape[:2]
# Displaying the height and width
print("Height = {}, Width = {}".format(h, w))
#print(image.shape)

# Extracting RGB values.
# Here we have randomly chosen a pixel
# by passing in 100, 100 for height and width.
(B, G, R) = image[100, 100]

# Displaying the pixel values
print("R = {}, G = {}, B = {}".format(R, G, B))

# We can also pass the channel to extract
# the value for a specific channel
B = image[100, 100, 0]
print("B = {}".format(B))

# We will calculate the region of interest
# by slicing the pixels of the image
# roi = image[100 : 500, 200 : 700]
# cv2.imshow("ROI", roi)
# cv2.waitKey(0)


# resize() function takes 2 parameters,
# the image and the dimensions
# resize = cv2.resize(image, (500, 500))
# cv2.imshow("Resized Image", resize)
# cv2.waitKey(0)

# We are copying the original image,
# as it is an in-place operation.
output = image.copy()

# Using the rectangle() function to create a rectangle.
rectangle = cv2.rectangle(output, (2, 5),
                        (10, 20), (255, 0, 0), 2)

# Copying the original image
output = image.copy()

# Adding the text using putText() function
text = cv2.putText(output, 'OpenCV Demo', (20, 20),
                cv2.FONT_HERSHEY_SIMPLEX, 4, (255, 0, 0), 2)
# cv2.imshow("Resized Image", rectangle)
# cv2.waitKey(0)
# sys.exit()
