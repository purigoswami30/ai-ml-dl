import cv2
import matplotlib.pyplot as plt 

imgTest = cv2.imread("tests.png")
pic = cv2.imread("pic.png")

print(pic.shape)
imgTest = cv2.resize(imgTest, (pic.shape[1], pic.shape[0])) 

#add
#imgAdd = cv2.add(imgTest, pic)
#imgAdd = cv2.subtract(imgTest, pic)
weightedSum = cv2.addWeighted(imgTest, 0.5, pic, 0.4, 0) 

# # Converting BGR color to RGB color format
# RGB_img = cv2.cvtColor(imgAdd, code=cv2.COLOR_BGR2HSV)

# #Displaying image using plt.imshow() method
# plt.imshow(imgAdd)

# #hold the window
# plt.waitforbuttonpress()
# plt.close('all')

# the window showing output image 
# with the weighted sum  
cv2.imshow('Weighted Image', weightedSum) 
  
# De-allocate any associated memory usage   
if cv2.waitKey(0) & 0xff == 27:  
    cv2.destroyAllWindows()  