# Python program to explain cv2.erode() method  
  
# importing cv2  
import cv2 
  
# importing numpy  
import numpy as np 
    
# Reading an image in default mode  
image = cv2.imread('tests.png')
  
# Window name in which image is displayed  
window_name = 'Image'
  
# Creating kernel 
kernel = np.ones((6, 6), np.uint8)
#np.array([[0, 0, 1], [0, 0, 1], [0, 0, 1]], np.uint8)#//np.zeros((6, 6), np.uint8) 
print(kernel)
  
# Using cv2.erode() method  
image = cv2.erode(image, kernel, cv2.BORDER_REFLECT)  
  
# Displaying the image  
cv2.imshow(window_name, image) 
cv2.waitKey(0)