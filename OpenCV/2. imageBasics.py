import cv2
import matplotlib.pyplot as plt

image = cv2.imread("tests.png", flags=1)
B, G, R = cv2.split(image) 

# Converting BGR color to RGB color format
RGB_img = cv2.cvtColor(image, code=cv2.COLOR_BGR2HSV)

#Displaying image using plt.imshow() method
plt.imshow(RGB_img)

#hold the window
plt.waitforbuttonpress()
plt.close('all')